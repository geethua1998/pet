from random import randrange
class Pet():
    bore_threshold=5
    hunger_threshold=7
    boredom_decrement=3
    sounds=["mm"]
    hunger_decrement=3
    def __init__(self,name):  #,hunger,boredom,sounds
        self.name=name
        self.hunger=randrange(self.hunger_threshold)
        self.boredom =randrange(self.bore_threshold)
        self.sounds=self.sounds[:]
    def clock_tick(self):
        self.hunger += 1
        self.boredom += 1
    def __str__(self):
        return self.name+" is feeling "+self.current_state()
    def current_state(self):
        if (self.hunger <= self.hunger_threshold) and (self.boredom <= self.bore_threshold):
            return "Happy!"
        elif self.hunger>self.hunger_threshold:
            return "Hungry!"
        elif (self.hunger>self.hunger_threshold) and (self.boredom>self.bore_threshold):
            return "Hungry and Bored!"
        else:
            return "Bored!"
    def teach(self,w):
        self.sounds.append(w)
        self.reduce_boredom()
    def hi(self):
        print(self.sounds[randrange(len(self.sounds))])
        self.reduce_boredom()
    def reduce_boredom(self):
        self.boredom=max(0,self.boredom-self.boredom_decrement)
        #return self.boredom
    def feed(self):
        self.reduce_hunger()
    def reduce_hunger(self):
        self.hunger=max(0,self.hunger-self.hunger_decrement)
        #return self.hunger

def menup(menuoptions):
    options=list(menuoptions.keys())
    print("----------")
    for i in options:
        print(i+":\t"+menuoptions[i]["t"])

petoptions = ["Husky dog","Persian cat","Hamster","German shepherd dog","Golden retriever","Beagle","Sphynx cat","Parakeet","Canaries","Rabit"]
choice=''
while choice.capitalize() not in petoptions:
    print("Choose a pet type from the following options")
    for opt in petoptions:
        print(opt)
    choice =input("Please select a pet to adopt : ")
name=input("Name your pet :")
menuoptions ={"F":{"function":"feed","t":"Feed pet"},"T":{"function":"teach","t":"Teach"},"G":{"function":"hi","t":"Greet"},"Q":{"function":"Quit","t":"Quit"}}
play=True
while play:
    pet=Pet(name)
    print(pet)
    for i in range(5):
        pet.clock_tick()
        print(pet)
    selection = ""
    while selection not in menuoptions.keys():
        menup(menuoptions)
        selection = input("What would you like to do?").upper()
    if menuoptions[selection]["function"]=="feed":
        pet.feed()
        print(name+" has been fed.Hunger reduced.")
    elif menuoptions[selection]["function"] == "hi":
        pet.hi()
    elif menuoptions[selection]["function"]=="teach":
        w=input("What word do you want to teach "+name)
        print("Taught the word "+w+" to "+name+". Boredom reduced.")
        pet.teach(w)
    else:
        break
    print(pet)













